
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;
class Keycheck implements Runnable{
	static boolean stai=false;//pentru pauza  
	static boolean gata=false;//pentru terminare
	Thread keycheck;
	Keycheck(){
		keycheck=new Thread(this,"twk-keycheck");
		keycheck.start();
	}
	public void run(){
		//System.out.println("goz2 start\n");
		Scanner kb = new Scanner(System.in);
		String w="";
		boolean bistabil=false;
		do{
		 w=kb.nextLine();
		 if(w.length()==0)
			 if(!bistabil){
				 w="p";
				 bistabil=true;
				 System.out.println("PAUSED!!"); }
			 else {
				w="c";
			 	bistabil=false;
			 	System.out.println("UN-PAUSED!!");};
		 		
		 stai=false;
		 if (w.charAt(0)=='p')stai=true; 
		}while(w.charAt(0)!='q');
		gata=true;
	};
}
public class twk {       //clasa programului
	static final int MAXX=1000;//marime harta km
	static final int MAXY=1000;
	static final int MIND=250; //distanta minima intre jucatori km
	static final int GRAN=10;// marime casuta elementara harta (grid) km
	Keycheck k=new Keycheck();
	static class player{
		int id;
		String Nume;
		public int x,y;
		public int raza;
		public int resurse;
		public int armata;
		public double armata_atac;
		public int land;
		public int border;
		public int sl_explore;
		public int sl_army;
		public int sl_attack;
		public int sl_expand;
		public int sl_rezerve;
		public int radar;
		public int d_raza;
		public int d_resurse;
		public int d_land;
		public int d_armata;
		public int armata_aparare;
		public String  news;
		player()
			{

			}
		void print()
			{
			//System.out.println("--"+Nume+" "+x+","+y+" "+raza+" "+resurse+" "+armata);
			//System.out.printf("%-8s |%3d| %4d %3d %n",Nume,raza,resurse,armata);
			System.out.printf("%-6s  %4d*  %4d#  %6d$  %3d@ | %3d %3d %3d %3d %3d |%n",Nume,raza,land,resurse,armata,sl_explore,sl_expand,sl_army,sl_attack,sl_rezerve);
			//System.out.printf("%-8s %3d %4d %3d %n",Nume,raza,resurse,armata);
			}
		
		};
	static class relatie{
		public int id1;
		public int id2;
		public String status1;
		public String status2;
		public int dist;
		public int land;
		public int armata;
		public int loses1;
		public int loses2;
		public int landgain;
		public int resgain;
		public int border;
		public int victory=0;
		public boolean contact=false;
		relatie(){		
		}
	}
	static void waiting (int n){
		        
		        long t0, t1;

		        t0 =  System.currentTimeMillis();

		        do{
		            t1 = System.currentTimeMillis();
		        }
		        while ((t1 - t0) < (n * 1000));
		    }
	static int dist(player p1,player p2)
	{
	 float dx=p1.x-p2.x;
	 if(dx*dx>MAXX*MAXX/4)dx=MAXX-Math.abs(dx);
	 float dy=p1.y-p2.y;
	 if(dy*dy>MAXY*MAXY/4)dy=MAXY-Math.abs(dy);
	 Double d=Math.sqrt(dx*dx+dy*dy);
	 return d.intValue();	
	}
	static class loc{
		int influenta=0;
		int owner=0;
		boolean schimbat=false;
	}
	static int dist(player p,int x,int y){
		float dx=p.x-x*GRAN;
		 if(dx*dx>MAXX*MAXX/4)dx=MAXX-Math.abs(dx);
		 float dy=p.y-y*GRAN;
		 if(dy*dy>MAXY*MAXY/4)dy=MAXY-Math.abs(dy);
		 Double d=Math.sqrt(dx*dx+dy*dy);
		 return d.intValue();		
	}
	static loc[][] map;
	private static final String dbClassName = "com.mysql.jdbc.Driver";
	private static final String CONNECTION="jdbc:mysql://dxubuntu/twk";
	public static void main(String[] args)throws ClassNotFoundException,SQLException
    	{
	
		String[] arguments = new String[] {""};
		twk0.main(arguments);
		Keycheck k = new Keycheck();
		k.toString();  // pt disablare warning no use of k
		int deltaid=0; //id primul element
		map=new loc[MAXX][MAXY];
		Class.forName(dbClassName);
		Properties prop = new Properties();
	    prop.put("user","root");
	    prop.put("password","pasword");
	    Connection c = DriverManager.getConnection(CONNECTION,prop);
	    Statement s = c.createStatement();
	    ResultSet r = s.executeQuery("select count(*) from pl_stats");
	    r.next();
	    int nr_playeri=r.getInt(1);	    
	    System.out.println("Connector works "+nr_playeri+"players found:");
	    r.close();
//---------------intializari tabele----------------------------	    
	    relatie[][] pvp=new relatie[nr_playeri][nr_playeri];
	    for(int i=0;i<nr_playeri;i++)
	    	for(int j=0;j<nr_playeri;j++)
	    	{
	    	pvp[i][j]=new relatie(); 
	    	}
	    player[] p= new player[nr_playeri];
	    map= new loc[MAXX/GRAN][MAXY/GRAN];
	    for(int i=0;i<MAXX/GRAN;i++)
	    	for(int j=0;j<MAXY/GRAN;j++)
	    	{
	    	map[i][j]=new loc(); 
	    	}
//========================================================	      
	      for(int t=1;t<1000;t++)
		{
	    	  long starttime=System.currentTimeMillis();
	    	  String q="UPDATE para SET tura=0";
		    	 s.executeUpdate(q);
    	  
//****************************READ t ******************	    
	    r = s.executeQuery("select * from pl_stats");   
	    int i=0,j=0;
	    while(r.next())
	    	{
	    	//System.out.println(i);
	    	if(p[i]==null)p[i]=new player();
	    	p[i].id=r.getInt("id");
	    	if(i==0)deltaid=p[i].id;   // <<<< ID primul jucator(0)
	    	p[i].Nume=r.getString("Nume");
	    	p[i].x=r.getInt("x");
	    	p[i].y=r.getInt("y");
	    	p[i].raza=r.getInt("raza");
	    	p[i].resurse=r.getInt("resurse");
	    	p[i].armata=r.getInt("armata");
	    	p[i].d_land=r.getInt("land");    //  !!!!!! valoare veche LAND
	    	p[i].sl_explore=r.getInt("sl_explore");	    	
	    	p[i].sl_attack=r.getInt("sl_attack");
	    	p[i].sl_expand=r.getInt("sl_expand");
	    	p[i].sl_rezerve=r.getInt("sl_rezerve");
	    	p[i].sl_army=r.getInt("sl_army");
	    	p[i].border=0;
	    	i++;
	    	}	    
	    r.close();
	    //--------------initializare influenta----------------------
	    for(i=0;i<MAXX/GRAN;i++)
	    	for(j=0;j<MAXY/GRAN;j++)
	    	{
	    	map[i][j].influenta=0;
	    	}
	    
	    //--------------initializare pvp------------------------------	    
	    for(i=0;i<nr_playeri;i++)
	    	for(j=0;j<nr_playeri;j++)
	    	{
	    	pvp[i][j].id1=p[i].id;
	    	pvp[i][j].id2=p[j].id;
	    	pvp[i][j].status1="pax";
	    	pvp[i][j].status2="pax";
	    	pvp[i][j].border=0;
	    	pvp[i][j].victory=0;
	    	pvp[i][j].dist=dist(p[i],p[j]);
	    	
	    	}
//      -----------------read pvp------------
	    
	    r = s.executeQuery("select * from pvp");   
	    while(r.next())
    	{
    	 i=r.getInt("id1")-deltaid;
    	 j=r.getInt("id2")-deltaid;
    	 pvp[i][j].status1=r.getString("status1");
    	 pvp[i][j].status2=r.getString("status2");    	 
    	}	    
	    r.close();
//  -------------------calcul border vertical---------------------- 
	    for(i=0;i<MAXX/GRAN;i++){
	    	int vecin=map[i][MAXY/GRAN-1].owner-deltaid;  //se incarca vecinul din capatul celalat al hartii
 	    	for(j=0;j<MAXY/GRAN;j++){
 	    		int localnic=map[i][j].owner-deltaid;
 	    		if(vecin!=localnic){
 	    			if(localnic!=-deltaid){  //daca locatia nu e barbara
 	    				p[localnic].border++;
 	    				if(vecin!=-deltaid){ //daca vecinul nu e barbar
 	    					p[vecin].border++;
 	    					pvp[localnic][vecin].border++;
 	    					pvp[vecin][localnic].border++; 	    					
 	    				}
 	    			}
 	    		}
 	    		vecin=localnic;  //se memoreaza pt tura urmatoare
	    	}
	     }
 //  -------------------calcul border orizontal---------------------- 
 		    for(j=0;j<MAXY/GRAN;j++){
 		    	int vecin=map[MAXX/GRAN-1][j].owner-deltaid;  //se incarca vecinul din capatul celalat al hartii
 	 	    	for(i=0;i<MAXX/GRAN;i++){
 	 	    		int localnic=map[i][j].owner-deltaid;
 	 	    		if(vecin!=localnic){
 	 	    			if(localnic!=-deltaid){  //daca locatia nu e barbara
 	 	    				p[localnic].border++;
 	 	    				if(vecin!=-deltaid){ //daca vecinul nu e barbar
 	 	    					p[vecin].border++;
 	 	    					pvp[localnic][vecin].border++;
 	 	    					pvp[vecin][localnic].border++; 	    					
 	 	    				}
 	 	    			}
 	 	    		}
 	 	    		vecin=localnic;  //se memoreaza pt tura urmatoare
 		    	}
 		    }	
	    
	    //********************t=t+1*****************************************
	    Random rnd = new Random();
	    
	    for(player x:p)
	   		{
	    	 //System.out.println("----------  "+x.Nume+"  -----------");
	    	 x.d_land=x.land;
	    	 x.land=0;
	    	 x.radar=0;
	    	 x.armata_atac=0;
	    	 x.news=" ";
	    	 for(i=0;i<MAXX/GRAN;i++)
	 	    	for(j=0;j<MAXY/GRAN;j++)
	 	    	{
	 	    	    int influenta=x.raza-dist(x,i,j);
	 	    	    if(influenta>map[i][j].influenta){
	 	    	    	if(map[i][j].owner==0){
	 	    	    		map[i][j].owner=x.id;
	 	    	    		map[i][j].schimbat=true;
	 	    	    		map[i][j].influenta=influenta;		 	    	    	
	 	    	    	}
	 	    	    	if(map[i][j].owner==x.id)map[i][j].influenta=influenta;
	 	    	    }		
	 	    		if(map[i][j].owner==x.id)x.land++; 
	 	    	}
	    	 
		     x.d_resurse=x.land-x.armata; //upkeep
		     x.resurse+=x.d_resurse;
		     x.d_armata=0;
		     if(x.resurse<0)
		     	{
		    	 x.news=x.news+"FOAME! <br>";
		    	 x.armata+=x.resurse;//mor unii de foame  ++
		    	 x.d_armata+=x.resurse;
		    	 x.d_resurse=-x.armata;//upkeep 1(acasa)
		    	 x.resurse=0;
		    	 if(x.armata<0)x.armata=0;  //au murit toti ++++++++
		   	 			    	 	
		     	}
		     int resurse_explorare=x.resurse*x.sl_explore/(x.sl_army+x.sl_rezerve+x.sl_explore+x.sl_attack+x.sl_expand+1);
		     int armata_explorare=resurse_explorare/25;
		     if(armata_explorare>=x.armata)  //se trimite toata armata in explorare
		     		{armata_explorare=x.armata;
		     		 resurse_explorare=x.armata*25;	// 25=cost spionaj/om	
		     		}
		     //System.out.println("spioni="+armata_explorare);
		     x.resurse-=resurse_explorare;
		     x.d_resurse-=resurse_explorare;
		     int pierderi_armata_explorare=armata_explorare/(9+rnd.nextInt(4));
		     x.radar=(int)Math.sqrt(40*resurse_explorare+x.raza*x.raza)-x.raza;
		     //System.out.println("radar="+x.radar);
		     int resurse_expandare=(x.resurse*x.sl_expand)/(x.sl_army+x.sl_rezerve+x.sl_explore+x.sl_attack+x.sl_expand+1);
		     int armata_expandare=(int)(resurse_expandare/12);
		     if(armata_expandare>=x.armata-armata_explorare)  //se foloseste toata armata RAMASA la infrastructura
		     		{armata_expandare=x.armata-armata_explorare;
		     		 resurse_expandare=(x.armata-armata_explorare)*12;// 12=cost infrastructura/om		
		     		}	
		     //System.out.println("lucratori="+armata_expandare);
		     //System.out.println("res_expandare"+resurse_expandare);
		     x.resurse-=resurse_expandare;
		     x.d_resurse-=resurse_expandare;
		     int pierderi_armata_expandare=armata_expandare/(6+rnd.nextInt(3));
		     int delta_raza=(int)Math.sqrt(10*resurse_expandare+x.raza*x.raza)-x.raza;
		     x.d_raza=(int)delta_raza;
		     x.raza+=(int)delta_raza;		      
		     int resurse_recrutare=x.resurse*x.sl_army/(x.sl_army+x.sl_rezerve+x.sl_explore+x.sl_expand+x.sl_attack+1);
		     if(resurse_recrutare>x.land/30*35)resurse_recrutare=x.land/30*35;//maxim recruti land/30
		     x.resurse-=resurse_recrutare;
		     x.d_resurse-=resurse_recrutare;
		     int atac_front=0;
		     for(j=0;j<nr_playeri;j++)if(pvp[x.id-deltaid][j].status1.equals("belum"))atac_front+=pvp[x.id-deltaid][j].border;

		     int resurse_atac=(x.resurse*x.sl_attack)/(x.sl_army+x.sl_rezerve+x.sl_explore+x.sl_attack+x.sl_expand+1);
		     int armata_atac=resurse_atac/5;
		     if(armata_atac>=x.armata-armata_explorare-armata_expandare)  //se foloseste toata armata in atac
	     		{armata_atac=x.armata-armata_explorare-armata_expandare;
	     		 resurse_atac=(x.armata)*5;// 5=cost atac/om	
	     		 //System.out.print("*");
	     		}	
		     if(atac_front>0){ 
		    	 x.resurse-=resurse_atac;
		    	 x.d_resurse-=resurse_atac;
			     }
		     //System.out.println("arm ata "+armata_atac);
		     if(atac_front>0)x.armata_atac=armata_atac/atac_front*x.border;
		     
		     //System.out.println("ataf"+atac_front+"arm ataf "+x.armata_atac);
		     x.armata_aparare=x.armata-armata_expandare/2-armata_explorare-armata_atac;
		     x.armata+=resurse_recrutare/35;
		     x.d_armata+=resurse_recrutare/35;
		     x.armata+= -pierderi_armata_explorare-pierderi_armata_expandare;
		     x.d_armata+= -pierderi_armata_explorare-pierderi_armata_expandare;
		     
	   		}
	    
	    //-------pvp-----------
	    for(i=0;i<nr_playeri;i++)
	    	for(j=0;j<nr_playeri;j++)
	    	 {
	    		if(i!=j)
	    			if(dist(p[i],p[j])<p[i].raza+p[i].radar+p[j].raza)
	    				{
	    				pvp[i][j].contact=true;
	    				pvp[i][j].victory=0;
	    				if(p[j].land==0)pvp[i][j].status1=" + ";
	    				if(pvp[i][j].status1.equals("belum")){
	    					double forta_asalt=(p[i].armata_atac*pvp[i][j].border)/(p[i].border+.01);
	    					double forta_aparare=p[j].armata_aparare;
	    					//if(pvp[j][i].status1.equals("belum"))forta_aparare+=(p[j].armata_atac*pvp[i][j].border)/(p[j].border+.01);
	    					System.out.println(p[i].Nume+forta_asalt+"->"+forta_aparare+p[j].Nume);
	    					int pierderi_atac=(int)Math.round((forta_aparare*13/(10.0+rnd.nextInt(15))*100/(pvp[i][j].border+25)));
	    					if(pierderi_atac>forta_asalt)pierderi_atac=(int)(forta_asalt);
	    					int pierderi_aparare=(int)Math.round((forta_asalt*10/(50.0+rnd.nextInt(30))));
	    					if(pierderi_aparare>forta_aparare)pierderi_aparare=(int)(forta_aparare);
	    					pvp[i][j].loses1+=pierderi_atac;
	    					pvp[j][i].loses2+=pierderi_atac;
	    					pvp[i][j].loses2+=pierderi_aparare;
	    					pvp[j][i].loses1+=pierderi_aparare;
	    					p[i].armata-=pierderi_atac;
	    					if(p[i].armata<0)p[i].armata=0;
	    					p[i].d_armata-=pierderi_atac;
	    					p[j].armata-=pierderi_aparare;
	    					if(p[j].armata<0)p[j].armata=0;
	    					p[j].d_armata-=pierderi_aparare;
	    					p[i].news+="Ofensiva->"+p[j].Nume+"A:"+(int)forta_asalt+"("+pierderi_atac+")--";
	    					p[j].news+="Defensiva->"+p[i].Nume+"A:"+(int)forta_asalt+"("+pierderi_atac+")--";
	    					if(forta_asalt>forta_aparare/5)p[i].news+="D:"+(int)forta_aparare+"("+pierderi_aparare+")";
	    					p[j].news+="D:"+(int)forta_aparare+"("+pierderi_aparare+")";
	    					
	    					double victory_score=(forta_asalt-pierderi_atac)/(1+forta_aparare-pierderi_aparare);
	    					if(victory_score>2){ //jaf
	    						int jaf_transport=(int) Math.round(4*(forta_asalt-pierderi_atac));
	    						int prada=(int) Math.round(p[j].resurse/p[j].border*pvp[i][j].border);
	    						if(jaf_transport<prada){
	    							pvp[i][j].resgain+=jaf_transport;
	    							pvp[j][i].resgain-=jaf_transport;
	    							p[i].resurse+=jaf_transport;
	    							p[j].resurse-=jaf_transport;	    							
	    						}else{
	    							pvp[i][j].resgain+=prada;
	    							pvp[j][i].resgain-=prada;
	    							p[i].resurse+=prada;
	    							p[j].resurse-=prada;	
	    							
	    						}
	    						//System.out.println("Jaf");
	    					if(victory_score>4){ //victorie totala 
	    							pvp[i][j].victory=1;
	    							int delta_r=(int) Math.round(p[j].raza*((pvp[i][j].border)+70)/1000);
	    							//p[i].raza+=delta_r/6; // castigi prestigiu
	    							p[j].raza-=delta_r;
	    							p[i].news+="Victorie";
	    							p[j].news+="Dezastru";
	    						}
	    					
	    					}	
	    					p[i].news+=" <br>";
							p[j].news+=" <br>";
	    					//System.out.println("Loses AD "+pierderi_atac+">>"+pierderi_aparare);
	    					System.out.println(p[i].news);
	    				}
	    				pvp[i][j].land=p[j].land;
	    				double eroare=((double)p[j].radar+10)/((double)p[i].radar+10)/4;
	    				//double semn=1000*rnd.nextDouble()-500;
	    				//if(semn!=0)semn=semn/Math.abs(semn);
	    				//System.out.println(p[j].radar+" "+p[i].radar+" "+eroare);
	    				pvp[i][j].armata=((int)(p[j].armata*(1+eroare)));
	    			}
	    		else pvp[i][j].contact=false;
	    	 }
	    for(player x:p)
   		{   	
    	 x.land=0;
    	 for(i=0;i<MAXX/GRAN;i++)
 	    	for(j=0;j<MAXY/GRAN;j++)
 	    	{
 	    	    int influenta=x.raza-dist(x,i,j);
 	    	    if(influenta>map[i][j].influenta){
 	    	    	if(map[i][j].owner==0){ 
 	    	    		map[i][j].owner=x.id;
 	    	    		map[i][j].schimbat=true;
 	    	    		map[i][j].influenta=influenta;
 	 	    	    	    
 	    	    	}
 	    	    	else if(pvp[x.id-deltaid][map[i][j].owner-deltaid].victory>0){//se schimba propietarul
 	    	    		map[i][j].influenta=influenta;	 	    	    	
 	    	    		pvp[x.id-deltaid][map[i][j].owner-deltaid].landgain++;
 	    	    		pvp[map[i][j].owner-deltaid][x.id-deltaid].landgain--;
 	    	    		map[i][j].owner=x.id;
 	    	    		map[i][j].schimbat=true;
 	    	    	}	
 	    	    }		
 	    		if(map[i][j].owner==x.id)x.land++; 
 	    	}
    	 x.d_land=x.land-x.d_land; //statistica land gain/turn
    	 //if(x.d_land>0)System.out.println(x.d_land+"# "+x.Nume);
   		}
	    //=======================calcul centru==================================
	    for(player w:p){
	    	int minx=0;  //cautare linie schimbare data x
	    	int minxval=100;
	    	for(i=0;i<MAXX/GRAN;i++){
	    		int yval=0;
	    		for(j=0;j<MAXY/GRAN;j++)
	    		 if(map[i][j].owner==w.id)yval++;
	    		if(yval<minxval){minxval=yval;minx=i;}
	    	}
	    	int miny=0; //cautare linie schimbare data y
            int minyval=100;
            for(j=0;j<MAXY/GRAN;j++){
            	int xval=0;
            	for(i=0;i<MAXX/GRAN;i++)
            	 if(map[i][j].owner==w.id)xval++;
            	if(xval<minyval){minyval=xval;miny=j;}
            }
            //System.out.println(w.Nume+minx+" "+miny);
            int xtot=0,ytot=0,gtot=0;
            for(i=0;i<MAXX/GRAN;i++)
            	for(j=0;j<MAXY/GRAN;j++)
            		if(map[i][j].owner==w.id){
            			int x,y;
            			gtot++;
            			y=j*GRAN-miny*GRAN;
            			x=i*GRAN-minx*GRAN;
            			if(y<0)y=MAXY+y;
            			if(x<0)x=MAXX+x;
            			xtot+=x;
            			ytot+=y;
            		}
            if(gtot>0){
            	w.x=xtot/gtot+minx*GRAN;
            	w.y=ytot/gtot+miny*GRAN;
            	if(w.y>MAXY)w.y=w.y-MAXY;
            	if(w.x>MAXX)w.x=w.x-MAXX;}			
	    }
	    
	    //********************WRITE t+1*****************************************
	    c.setAutoCommit(false);
	    for(i=0;i<nr_playeri;i++)
	    	{
	    	 q="UPDATE pl_stats SET";
	    	 q=q+" raza="+p[i].raza;
	    	 q=q+", x="+p[i].x;
	    	 q=q+", y="+p[i].y;
	    	 q=q+", resurse="+p[i].resurse;
	    	 q=q+", armata="+p[i].armata;
	    	 q=q+", radar="+p[i].radar;
	    	 q=q+", land="+p[i].land;
	    	 q=q+", d_land="+p[i].d_land;
	    	 q=q+", d_raza="+p[i].d_raza;
	    	 q=q+", d_resurse="+p[i].d_resurse;
	    	 q=q+", d_armata="+p[i].d_armata;
	    	 q=q+", sl_explore="+p[i].sl_explore;
	    	 q=q+", sl_expand="+p[i].sl_expand;
	    	 q=q+", sl_attack="+p[i].sl_attack;
	    	 String newsw=p[i].news;
	    	 if(p[i].news.length()>250) newsw=p[i].news.substring(0, 250);
	    	 q=q+", news='"+newsw;
	    	 q=q+"' WHERE id="+p[i].id;
	    	 s.executeUpdate(q);
	    	 p[i].print();
	    	}
	    c.commit();
	    //System.out.println(System.currentTimeMillis()-starttime);
	    for(j=0;j<MAXY/GRAN;j++)
	    	for(i=0;i<MAXX/GRAN;i++){
	    		q="UPDATE loc SET owner="+map[i][j].owner+" WHERE id="+(1+i+j*MAXX/10);
	    		//System.out.println(q);
	    		if(map[i][j].schimbat){
	    			s.executeUpdate(q);
	    			map[i][j].schimbat=false;
	    		}
	    		
	    	}
	    c.commit();	
	    //System.out.println(System.currentTimeMillis()-starttime+"ms");
	    //salvare pvp
	    s.executeUpdate("DELETE FROM pvp");	
	    for(i=0;i<nr_playeri;i++)
	    	for(j=0;j<nr_playeri;j++)
	    		if(pvp[i][j].contact)
	    		{
	    		
	    		 q="INSERT INTO pvp (id1,id2,status1,status2,dist,land,armata,loses1,loses2,landgain,border,resgain) VALUES('";
	   	    	 q=q+pvp[i][j].id1+"','";
	   	    	 q=q+pvp[i][j].id2+"','";
	   	    	 q=q+pvp[i][j].status1+"','";
	   	    	 q=q+pvp[i][j].status2+"','";
	   	    	 q=q+pvp[i][j].dist+"','";
	   	     	 q=q+pvp[i][j].land+"','";
	   	    	 q=q+pvp[i][j].armata+"','";
	   	    	 q=q+pvp[i][j].loses1+"','";
	   	    	 q=q+pvp[i][j].loses2+"','";
	   	    	 q=q+pvp[i][j].landgain+"','";
	   	    	 q=q+pvp[i][j].border+"','";
	   	    	 q=q+pvp[i][j].resgain+"')";
	   	    	
	   	    	 //long t0=System.currentTimeMillis();		    		
	   	    	 s.executeUpdate(q);
	   	    	// System.out.println("#"+(System.currentTimeMillis()-t0));
	    			
	    		}
	     q="UPDATE para SET tura="+t;
	     s.executeUpdate(q);
	     c.commit();
	     c.setAutoCommit(true);
	    System.out.print("-------------"+t+"--------------");
	   // waiting(50);
	    System.out.println(System.currentTimeMillis()-starttime+"ms");
	    try{
	       Thread.sleep(10000-System.currentTimeMillis()+starttime);
	       while(Keycheck.stai)Thread.sleep(1000);
           if(Keycheck.gata)break;
    	}
	    catch(InterruptedException ie){} 
		}  //aici se termina marele for tura
	    s.close();
	    c.close();
	    System.out.println("Bye");
    	} //aici se termina main
	
}//aici se termina twk

