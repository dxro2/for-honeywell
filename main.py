import pygame
import numpy as np
import random

print(f'Start')
pygame.init()
screen = pygame.display.set_mode((1920, 1080))
pygame.display.set_caption("North vs South")
icon = pygame.image.load("icon.png")
pygame.display.set_icon(icon)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
BLACK = (0, 0, 0)
GREEN = (0, 255, 0)
GRAY = (40, 20, 20)
BROWN = (188, 155, 144)
YELLOW = (255, 255, 0)
font = pygame.font.Font(pygame.font.get_default_font(), 12)
clock = pygame.time.Clock()
MAXX = 56
MAXY = 32
running = True
scale = 32
mapSurface = pygame.Surface((MAXX * scale, MAXY * scale))


class PlaceClass:
  def __init__(self, xx, yy, land):
    self.x = xx
    self.y = yy
    self.land = land

  def draw(self):
    color = BLUE  # sea
    if self.land == 1:
      color = GREEN  # grass
    elif self.land == 8:
      color = YELLOW  # hill
    elif self.land == 64:
      color = BROWN  # mountain
    pygame.draw.rect(mapSurface, color, (self.x * scale, self.y * scale, scale, scale), 0)


class Entity:
  owner = 0  # neutral 1=white/nord 2=black/south
  x = 0
  y = 0
  unitType = 0 # 1=civil 2=inf 3=cav 4=arty 5=aero 6=base 7=radar 8=ship
  chassis =0 # 1=foot 2=traked 3=aero 4=deployable 5=structure
  weight =0 # can be loaded on transport
  capacity =0 #can transport
  maxfuel=0 #max fuel capacity
  fuel=0 #current fuel
  ammo=0 #current ammo (max depend on capacity)
  util=0 #can build/repair
  hp=100 #hitpoints 100=as new

Landscape = np.empty((MAXX, MAXY), dtype=PlaceClass)
for j in range(MAXY):
  for i in range(MAXX):
    Landscape[i, j] = PlaceClass(i, j, 0)


def sumAlttitudeAround(xx, yy):
  if xx < MAXX - 1:
    xm = xx + 1
  else:
    xm = xx
  if xx > 0: xx -= 1
  if yy < MAXY - 1:
    ym = yy + 1
  else:
    ym = yy
  if yy > 0: yy -= 1
  sumalt = 0
  for ii in range(xx, xm + 1):
    for jj in range(yy, ym + 1):
      sumalt += Landscape[ii, jj].land
  return sumalt


for flip in range(2):
  for i in range(30):  # place (30)mountains
    while True:
      x = random.randint(0, MAXX - 1)
      y = random.randint(0, int(MAXY / 2) - 8)
      if flip == 1:
        y = MAXY - y - 1
        validator = random.randint(0, 100) + sumAlttitudeAround(x, y) * 20 - i * 12 - 2 * abs(
          MAXX / 2 - x) - 3 * (MAXY - y)
      else:
        validator = random.randint(0, 100) + sumAlttitudeAround(x, y) * 20 - i * 12 - 2 * abs(
          MAXX / 2 - x) - 3 * y
      if Landscape[x, y].land == 0 and validator > 0:
        Landscape[x, y].land = 64
        break

  for i in range(120):  # place (120)hills
    tentatives = 0
    while tentatives < 5000:
      x = random.randint(0, MAXX - 1)
      y = random.randint(0, int(MAXY / 2) - 4)
      if flip == 1:
        y = MAXY - y - 1
        validator = sumAlttitudeAround(x, y) - 60 + tentatives / 10
      else:
        validator = sumAlttitudeAround(x, y) - 60 + tentatives / 10
      if Landscape[x, y].land == 0 and validator > 0:
        Landscape[x, y].land = 8
        tentatives = 0
        break
      tentatives += 1

  for i in range(250):  # place (250)plains
    tentatives = 0
    while tentatives < 20000:
      x = random.randint(0, MAXX - 1)
      y = random.randint(0, int(MAXY / 2) - 1)
      if flip == 1:
        y = MAXY - y - 1
        validator = sumAlttitudeAround(x, y) - 50 + tentatives / 30
      else:
        validator = sumAlttitudeAround(x, y) - 50 + tentatives / 30
      if Landscape[x, y].land == 0 and validator > 0:
        Landscape[x, y].land = 1
        tentatives = 0
        break
      tentatives += 1

for row in Landscape:
  for cell in row:
    cell.draw()

while running:
  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      running = False
  mouse = pygame.mouse.get_pos()
  clock.tick(60)
  screen.fill(GRAY)
  screen.blit(mapSurface, (0, 20))
  textsurface = font.render(f"{clock.get_fps():{4}.{4}}", False, WHITE)
  screen.blit(textsurface, (0, 0))
  pygame.display.update()
